
import java.util.Scanner;

class Water{

    public static void main(String []argv){
	int minutes = input();
	System.out.println("You use " + minutes*4 + " bottles.");
    }
    /*
    function for input date
    @param 
    @ correct data
    */
    public static int input(){
	Scanner in = new Scanner(System.in);
	String str;
	do{
	    System.out.println("Input bottles:");
	    str = in.nextLine();
	}while(!isInt(str));
	return Integer.parseInt(str);
    }
    /*
    function for check data
    @param string
    @return true/false
    */
    public static boolean isInt(String str){
	int i = 0;
	while(i < str.length()){
	    switch(str.charAt(i)){
		case '0':
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9': i++;
		    break;
		default:
		    return false;
	    }
	}
	return true;
    }
}